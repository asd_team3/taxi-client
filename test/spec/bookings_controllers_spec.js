'use strict';

describe('LoginController', function() {

    beforeEach(module('taksi_client'));

    var LoginController,
        AuthService,
        AuthInterceptor,
        scope,
        //location,
        $httpBackend;

    beforeEach(inject(function ($controller, $rootScope,_$httpBackend_,_AuthService_,_AuthInterceptor_) {
        scope = $rootScope.$new();
        //location = $rootLocation.$new();
        $httpBackend = _$httpBackend_;

        LoginController = $controller('LoginController'), {
            $scope: scope,
            AuthService: _AuthService_,
            AuthInterceptor: _AuthInterceptor_,
            //location: $location
        }
    }));

    it('should login', function(){
        $httpBackend
            .expectPOST('http://localhost:3000/auth',
            {username: "user1@example.com", password: "user1234"})
            .respond(200);
        scope.username = "user1@example.com";
        scope.password = "user1234";
        //location.path = "/bookings";
        scope.submit();
        $httpBackend.flush();
    });

});

describe('BookingsCtrl', function () {

    beforeEach(module('taksi_client'));

    var BookingsCtrl,
        scope,
        $httpBackend;

    beforeEach(inject(function ($controller, $rootScope, _$httpBackend_, _BookingsService_, _PusherService_) {
        scope = $rootScope.$new();
        $httpBackend = _$httpBackend_;

        BookingsCtrl = $controller('BookingsCtrl', {
            $scope: scope,
            BookingsService: _BookingsService_,
            PusherService: _PusherService_
        });
    }));

    it('should submit a request to the backend service', function () {
        $httpBackend
            .expectPOST('http://localhost:3000/bookings',
            {phone: "1234", latitude: 58.37, longitude: 26.71})
            .respond(201, {message: 'Booking is being processed'});

        scope.latitude = 58.37;
        scope.longitude = 26.71;
        scope.phone = "1234";
        scope.submit();
        $httpBackend.flush();

        var stub = Pusher.instances[0];
        var channel = stub.channel('bookings');
        channel.emit('async_notification', {message: 'Your taxi will arrive in 3 minutes'})

        expect(scope.syncNotification).toBe('Booking is being processed');
        expect(scope.asyncNotification).toBe('Your taxi will arrive in 3 minutes');
    });
});

var app = angular.module('taksi_client', ['ngRoute','ngResource','$strap.directives']);

app.config(function($routeProvider){
    $routeProvider
        .when('/', {
            templateUrl: 'js/views/auth/login.html',
            controller: 'LoginController'
        })
        .when('/bookings', {
            templateUrl: 'js/views/bookings/new.html',
            controller: 'BookingsCtrl'
        })
        .when('/login', {
            templateUrl: 'js/views/auth/login.html',
            controller: 'LoginController'
        })
        .otherwise({
            redirectTo: '/'
        });
});

app.config(function($httpProvider) {
    return $httpProvider.interceptors.push("AuthInterceptor");
});
/*
$rootScope.$on("AuthEvents".notAuthorized, function() {
    $window.location.href = '/auth/login.html';
});
*/
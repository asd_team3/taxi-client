'use strict'

var app = angular.module('taksi_client');

app.controller('BookingsCtrl', function ($scope, BookingsService, PusherService, RequestsService) {
  $scope.latitude = undefined;
  $scope.longitude = undefined;
  $scope.phone = "";
  $scope.syncNotification = false;
  $scope.asyncNotification = '';
  $scope.isDisabled = false;
  navigator.geolocation.getCurrentPosition(function (position) {
    //console.log("I am inside");
    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;
    var mapProp = {
      center: {lat: latitude, lng: longitude},
      zoom: 14,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    $scope.map = new google.maps.Map(document.getElementById('map'), mapProp);
    //$scope.marker = new google.maps.Marker(
      //  {map: map, position: mapProp.center});

    $scope.$apply(function () {
      $scope.latitude = latitude;
      $scope.longitude = longitude;
    });

    // Create the search box and link it to the UI element.
    var input = document.getElementById('address');
    var searchBox = new google.maps.places.SearchBox(input);
    //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    $scope.map.addListener('bounds_changed', function () {
      searchBox.setBounds($scope.map.getBounds());
    });

    var markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function () {
      var places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }

      markers.forEach(function (marker) {
        marker.setMap(null);
      });
      markers = [];

      // For each place, get the icon, name and location.
      var bounds = new google.maps.LatLngBounds();
      places.forEach(function (place) {
        var icon = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25)
        };

        // Create a marker for each place.
        markers.push(new google.maps.Marker({
          map: $scope.map,
          icon: icon,
          title: place.name,
          position: place.geometry.location
        }));

        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      $scope.map.fitBounds(bounds);
    });

    $('<div/>').addClass('centerMarker').appendTo($scope.map.getDiv());

    google.maps.event.addListener($scope.map, 'idle', function () {
      var center = $scope.map.getCenter();
      document.getElementById('lat').value = center.lat();
      document.getElementById('lng').value = center.lng();
      $scope.latitude = center.lat();
      $scope.longitude = center.lng();
    } )

  });

    $scope.submit = function() {
        BookingsService.save({latitude: $scope.latitude, longitude: $scope.longitude, phone: $scope.phone}, function (response) {
            var id = response.id;
            PusherService.subscribe($scope, id);
        });
      $scope.syncNotification = true;
      $scope.isDisabled = true;
    };

  $scope.reloadPage = function(){window.location.reload();}
});
'use strict'

var app = angular.module('taksi_client');

app.controller('LoginController', function ($scope, AuthService, $location) {
    $scope.username = 'user1@example.com';
    $scope.password = 'user1234';
    $scope.submit = function() {
        var resp = AuthService.login($scope.username, $scope.password);
        $location.path('/bookings');
    };

});
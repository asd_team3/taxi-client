'use strict';

var app = angular.module('taksi_client');

app.service('BookingsService', function ($resource) {
  return $resource('http://limitless-retreat-9980.herokuapp.com/bookings', {});
  //return $resource('http://localhost:3000/bookings', {});
});
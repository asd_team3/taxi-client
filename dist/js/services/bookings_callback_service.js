'use strict';

var app = angular.module('taksi_client');

app.service('PusherService', function ($rootScope, RequestsService) {
    var service = this;
    var pusher = new Pusher('e0962ccdf017812e7264');
    var channel, channelID;
    //var to_unsub = undefined;

    service.subscribe = function($scope, bookingID){
        var breakpoint = false;
        channelID = '' + bookingID;
        channel = pusher.subscribe(channelID);

        channel.bind("confirmed", function(data) {
            $rootScope.$apply(function () {
                //to_unsub = channelID;
                $scope.asyncNotification = "is confirmed!";
                RequestsService.update($scope,bookingID,breakpoint);
            });
        });

        channel.bind("rejected_no_confirmations", function(data) {
            $rootScope.$apply(function () {
                pusher.unsubscribe(channelID);
                $scope.asyncNotification = " is rejected. Our drivers have not confirmed your booking!";
            });
        });

        channel.bind("rejected_no_taxis", function(data) {
            $rootScope.$apply(function () {
                pusher.unsubscribe(channelID);
                $scope.asyncNotification = " is rejected. No taxis are available!";
            });
        });

        channel.bind("completed", function(data) {
            breakpoint = true;
            $rootScope.$apply(function () {
                pusher.disconnect();
                //pusher.unsubscribe(channelID);
                //pusher.unsubscribe(to_unsub);
                $scope.asyncNotification = " is completed! Have a nice day!";
                RequestsService.update($scope,bookingID,breakpoint);
            });
        });
    };
    return service;
});
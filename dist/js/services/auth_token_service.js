'use strict';

var app = angular.module('taksi_client');

app.service("AuthToken", function() {
    return {
        get: function() {
            return localStorage.getItem('token');
        },
        set: function(token) {
            return localStorage.setItem('token',token);
        }
    }
});
